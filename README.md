
# xor-tree
A repository for the [xor-tree](http://codeforces.com/problemset/problem/429/A) problem.

## Running
```shell
$ make exe
$ ./exe < test03
```

